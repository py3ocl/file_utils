#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys

sys.path.append("..")

import os
from FileFinder import FileFinder

import unittest

class TestFileFinderMethods(unittest.TestCase):

    def test_FindFiles(self):
        ff = FileFinder("test_folder", True)

        self.assertTrue(os.path.join("test_folder", "test_file_1.txt") in ff.files)
        self.assertTrue(os.path.join("test_folder", "test_sub_folder") in ff.files)
        self.assertTrue(os.path.join("test_folder", "test_sub_folder", "test_file_2.txt") in ff.files)

    def test_Clear(self):
        ff = FileFinder("test_folder", True)
        ff.Clear()

        self.assertTrue(not ff.files)

if __name__ == '__main__':
    unittest.main()
