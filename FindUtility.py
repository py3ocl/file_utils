#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class FindUtility:

    ## Underscore character or alphanumeric character.
    @staticmethod
    def IsWordChar(ch):
        return ch.isalnum() or ch == "_"

    ## Examine word in line to see if it's separated by word character.
    @staticmethod
    def IsWord(pos, line, search):
        search_len = len(search)
        line_len = len(line)

        if line_len == search_len:
            return True # line contains exact match.
        else:
            if (pos == 0):
                is_pre_word_char  = False
                is_post_word_char = FindUtility.IsWordChar(line[search_len])
            elif (pos == line_len - search_len):
                is_pre_word_char  = FindUtility.IsWordChar(line[pos - 1])
                is_post_word_char = False
            else:
                is_pre_word_char  = FindUtility.IsWordChar(line[pos - 1])
                is_post_word_char = FindUtility.IsWordChar(line[pos + search_len])
            return not (is_pre_word_char or is_post_word_char)

    ## Helper function to find whole word within a line.
    @staticmethod
    def FindWord(start, line, search):
        pos = line.find(search, start)
        while pos != -1:
            if FindUtility.IsWord(pos, line, search):
                return True
            return FindUtility.FindWord(pos + len(search), line, search)
        return False

    ## Search for matching string within line, and optionally perform
    #  case sensitive and whole word matching.
    def FindInLine(line, search, match_case, whole_word):
        if match_case:
            if whole_word:
                return FindUtility.FindWord(0, line, search)
            return line.find(search) != -1
        if whole_word:
            # Perform search and ignore case.
            return FindUtility.FindWord(0, line.lower(), search.lower())
        return line.lower().find(search.lower()) != -1
