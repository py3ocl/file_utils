#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

if __name__ == '__main__':

    filename = sys.argv[1]
    output   = sys.argv[2]

    if len(sys.argv) != 3:
        print("Expecting input and output filename")
        sys.exit(1)

    lines = []
    data = None
    try:
        with open(filename) as f:
            data = f.read()
    except:
        print("Cannot read file {}".format(filename))
        data = None

    if data:
        for line in data.split("\n"):
            l = line[:-1] if line.endswith('\r') else line
            lines.append(l)
        lines.sort()

        try:
            with open(output, "w") as f:
                for l in lines:
                    f.write("{}\n".format(l))
        except:
            print("Cannot write file {}".format(output))
    else:
        print("No data")
