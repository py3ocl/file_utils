#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import threading

class AtomicInteger:

    def __init__(self, value = 0):
        self._value = value
        self.lock = threading.Lock()

    @property
    def value(self):
        self.lock.acquire()
        value = self._value
        self.lock.release()
        return value

    ## Set the count to a given value.
    @value.setter
    def value(self, value):
        assert(type(value) is int)
        self.lock.acquire()
        self._value = value
        self.lock.release()

    ## Increment the count and return the new value.
    def Inc(self, inc_by = 1):
        assert(type(inc_by) is int)
        self.lock.acquire()
        self._value += inc_by
        new_count = self._value
        self.lock.release()
        return new_count

    ## Decrement the count and return the new value.
    def Dec(self, dec_by = 1):
        assert(type(dec_by) is int)
        self.lock.acquire()
        self._value -= dec_by
        new_count = self._value
        self.lock.release()
        return new_count

    ## Set the count to the min value of current value or value.
    def SetMin(self, value):
        assert(type(value) is int)
        self.lock.acquire()
        self._value = min(value, self._value)
        self.lock.release()

    ## Set the count to the max value of current value or value.
    def SetMax(self, value):
        assert(type(value) is int)
        self.lock.acquire()
        self._value = max(value, self._value)
        self.lock.release()

    ## Thread-safe is less than comparison.
    ## True when count is less than value.
    def IsLess(self, value):
        assert(type(value) is int)
        self.lock.acquire()
        is_less = self._value < value
        self.lock.release()
        return is_less

    ## Thread-safe is greater than comparison.
    ## True when count is greater than value.
    def IsGreater(self, value):
        assert(type(value) is int)
        self.lock.acquire()
        is_greater = self._value > value
        self.lock.release()
        return is_greater
