#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class FileUtility:

    ## Convert binary data read from file to a list of text lines.
    @staticmethod
    def GetLines(data):
        lines = []
        for line in data.split("\n"):
            lines.append(line[:-1] if line.endswith('\r') else line)
        return lines

    ## Read a file as binary and return the data as a binary decoded string.
    @staticmethod
    def ReadFile(filename):
        e = None
        data = None
        try:
            with open(filename, "rb") as f:
                data = f.read()
            data = data.decode()
        except OSError as oe:
            data = None
            e = "error code {}".format(oe.errno).strip()
        except UnicodeDecodeError as ue:
            data = None
            e = "{} (encoding {})".format(ue.reason, ue.encoding).strip()
        except:
            data = None
            e = "error code {}".format(os.errno)

        lines = FileUtility.GetLines(data) if data else []
        return (lines, e)
