#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import threading
import multiprocessing

class Threads:

    ## Initialise the threads lock and empty list.
    def __init__(self):
        self.threads = []
        self.thread_lock = threading.Lock()

    ## Join all the threads in the list.
    def Join(self):
        self.thread_lock.acquire()
        threads = self.threads
        self.threads = []
        self.thread_lock.release()
        for t in threads:
            t.join()

    ## Get the number of threads in the list.
    def GetCount(self):
        self.thread_lock.acquire()
        count = len(self.threads)
        self.thread_lock.release()
        return count

    ## Create a thread name using startswith string and number of threads + 1.
    def GetThreadName(self, startswith=None):
        if startswith is None:
            startswith = "Thread-"
        self.thread_lock.acquire()
        count = len(self.threads) + 1
        name = "{}{}".format(startswith, count)
        self.thread_lock.release()
        return name

    ## Add a thread object to the list of threads.
    def Add(self, thread, start = True):
        self.thread_lock.acquire()
        self.threads.append(thread)
        self.thread_lock.release()
        if start:
            thread.start()
