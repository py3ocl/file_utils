#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import re
import time
from FindUtility import FindUtility
from FileUtility import FileUtility

class FindInFiles:

    ## Initialise and perform find in files.
    def __init__(self,
                 path,
                 search,
                 line_func = None, # Function called with matched line.
                 is_regex = False,
                 match_case = False,
                 whole_word = False,
                 recursive = True):
        self.lines = []
        self.line_count = 0
        self.total_files = 0
        self.longest_line = 0
        self.search = search

        # If line_func is a string, then this is a log filename.
        if type(line_func) is str:
            self.line_func = None
            self.log_filename = line_func
        else:
            self.line_func = line_func
            self.log_filename = None

        self.is_regex = is_regex
        self.match_case = match_case
        self.whole_word = whole_word
        self.failed_files = []
        self.pattern = re.compile(search) if is_regex else None

        if self.log_filename:
            self.log_file = open(self.log_filename, "wb")
        else:
            self.log_file = None

        if self.line_func:
            self.write_func = self.line_func
            self.encode = False
        elif self.log_file:
            self.write_func = self.log_file.write
            self.encode = True # When writing to log file, lines are encoded as binary.
        else:
            self.write_func = self.lines.append
            self.encode = False

        self.FindFiles(path, recursive)

    ## Ensure any log file is closed on exit.
    def __del__(self):
        if self.log_file:
            self.log_file.close()

    ## Clear the list of files.
    def Clear(self):
        self.lines = []

    ## Search for matching string within line, and optionally perform
    #  case sensitive and whole word matching.
    def FindInLine(self, line):
        return FindUtility.FindInLine(line, self.search, self.match_case, self.whole_word)

    ## Format using standard IDE method of formatting search results.
    @staticmethod
    def FormattedLine(filename, line_no, line, line_sep):
        return "{}({}):{}{}".format(filename,
                                    line_no,
                                    line[:-1] if line.endswith('\r') else line,
                                    line_sep)

    ## Record line by adding to self.lines or calling self.line_func.
    def RecordLines(self, filename, search_lines):
        success = True

        for line in search_lines:
            try:
                self.write_func(line.encode() if self.encode else line)
                self.line_count += 1
            except UnicodeEncodeError as e:
                success = False
            finally:
                pass

        return success

    ## Convert lines read from a file to lines matching the search and formatted for output.
    def GetSearchLines(self, filename, lines):
        search_lines = []
        find_func = self.pattern.search if self.is_regex else self.FindInLine
        line_sep = os.linesep if self.log_file is not None else ""
        if lines:
            for line_no, line in enumerate(lines):
                if find_func(line):
                    self.longest_line = max(len(line), self.longest_line)
                    search_lines.append(
                        FindInFiles.FormattedLine(filename, line_no + 1, line, line_sep))
        return search_lines

    ## Search for string within the filename specified.
    def SearchFile(self, filename):
        lines, e = FileUtility.ReadFile(filename)
        if not e and lines:
            search_lines = self.GetSearchLines(filename, lines)
            if search_lines and self.RecordLines(filename, search_lines):
                self.total_files += 1

    ## File files and folders, which can optionally be done recursively.
    def FindFiles(self, path, recursive = True):

        files = []
        try:
            files = os.listdir(path)
        except:
            self.failed_files.append(path)
            files = []
        for f in files:
            file_path = os.path.join(path, f) if path and path != "." else f
            if os.path.isdir(file_path):
                if f != '..' and f != '.':
                    if recursive:
                        self.FindFiles(file_path, True)
            else:
                self.SearchFile(file_path)

## Available command line options
OPT_HELP       = "--help"
OPT_RECURSIVE  = ["-r", "--recursive"]
OPT_REGEX      = "--regex"
OPT_MATCH_CASE = "--match-case"
OPT_WHOLE_WORD = "--whole-word"
OPT_SUMMARY    = "--summary"
OPT_LOG        = "--log"

## Compare a string or list of options.
def CompareOption(arg, options):
    if type(options) is list:
        return arg in options
    return arg == options

def PrintHelp():
    print("find_inf_files.py search [{} or {}] [{}] [{}] [{}] [{}] [{}=filename]".format(
        OPT_RECURSIVE[0], OPT_RECURSIVE[1], OPT_REGEX,
        OPT_MATCH_CASE, OPT_WHOLE_WORD, OPT_SUMMARY, OPT_LOG))

def no_print(msg):
    pass

if __name__ == '__main__':

    search = ""
    argv = sys.argv[1:]
    recursive   = False
    is_regex    = False
    match_case  = False
    whole_word  = False
    summary     = False
    log_file    = None

    if not argv:
        sys.stderr.write("ERROR: Search string or pattern expected\n")
        sys.exit(1)

    search = argv.pop(0)
    if CompareOption(search, OPT_HELP):
        PrintHelp()
        sys.exit(0)

    for arg in argv:
        if CompareOption(arg, OPT_HELP):
            PrintHelp()
            sys.exit(0)
        if arg.startswith(OPT_LOG + "="):
            log_file = arg[len(OPT_LOG) + 1:]
        elif CompareOption(arg, OPT_RECURSIVE):
            recursive = True
        elif CompareOption(arg, OPT_REGEX):
            is_regex = True
        elif CompareOption(arg, OPT_MATCH_CASE):
            match_case = True
        elif CompareOption(arg, OPT_WHOLE_WORD):
            whole_word = True
        elif CompareOption(arg, OPT_SUMMARY):
            summary = True

    recursive   = True
    match_case  = True
    whole_word  = True
    summary     = True

    start_time = time.perf_counter()
    fif = FindInFiles(".", search, log_file if log_file else print, is_regex, match_case, whole_word, recursive)
    end_time = time.perf_counter()

    if summary:
        print("Number of files {}".format(fif.total_files))
        print("Number of lines {}".format(fif.line_count))
        print("Longest line is {} characters".format(fif.longest_line))
        print("Elapsed time {}".format(end_time - start_time))
