#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys

sys.path.append("../list_utils")

import os
from ListUtility import ListUtility

class FileDiff:

    ## Read two files and perform the differences as a list.
    @staticmethod
    def GetDiff(filename1, filename2, linesep = None, left = True, right = True, verbose = False):
        if linesep is None:
            linesep = os.linesep

        f1 = open(filename1, "rb")
        file_data1 = f1.read().decode()
        f1.close()

        f2 = open(filename2, "rb")
        file_data2 = f2.read().decode()
        f2.close()

        lines1 = file_data1.split(linesep)
        lines2 = file_data2.split(linesep)

        if verbose:
            diff_func = ListUtility.GetVerboseDiffS
        elif left:
            diff_func = ListUtility.GetDiffS if right else ListUtility.GetLeftDiffS
        else:
            diff_func = ListUtility.GetRightDiffS

        return diff_func(lines1, lines2)

OPT_HELP    = "--help"
OPT_LEFT    = "--left"
OPT_RIGHT   = "--right"
OPT_VERBOSE = "--verbose"

def PrintHelp():
    print("FindDiff.py <first file> <second file> [{}] [{}] [{}] [{}]".format(
        OPT_HELP, OPT_LEFT, OPT_RIGHT, OPT_VERBOSE))

if __name__ == '__main__':

    filenames = []
    contains = []
    left = True
    right = True
    verbose = False

    for arg in sys.argv[1:]:
        if arg.startswith("--"):
            if arg == OPT_LEFT:
                right = False
            elif arg == OPT_RIGHT:
                left = False
            elif arg == OPT_VERBOSE:
                verbose = True
            elif arg == OPT_HELP:
                PrintHelp()
                sys.exit(0)
        else:
            filenames.append(arg)

    if len(filenames) >= 2:
        for l in FileDiff.GetDiff(filenames[0], filenames[1], os.linesep, left, right, verbose):
            if verbose:
                left_pos = l[0]
                right_pos = l[1]
                line = l[2]
                print("{}: {}".format(" left" if left_pos else "right", line))
            else:
                print(l)
