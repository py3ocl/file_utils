#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import time
import threading
import multiprocessing
from threads import Threads
from AtomicInteger import AtomicInteger

class ThreadedFileFinder:

    def __init__(self, path, recursive = True, max_threads = 0):
        self.files = []
        self.failed_files = []

        self.files_lock = threading.Lock()
        self.threads = Threads()
        self.max_threads = max_threads if max_threads >= 1 else multiprocessing.cpu_count() - 1
        self.thread_count = AtomicInteger()

        self.FindFiles(path, recursive)

    def Join(self):
        self.threads.Join()

    ## Clear the list of files.
    def Clear(self):
        self.files_lock.acquire()
        self.files = []
        self.files_lock.release()

    ## Append filename to list of files.
    def Append(self, path):
        self.files_lock.acquire()
        if type(path) is list:
            self.files.extend(path)
        else:
            self.files.append(path)
        self.files_lock.release()

    ## Add files recursively in a separate thread.
    def ThreadFindFiles(self, path, recursive = True):
        name = self.threads.GetThreadName()
        t = threading.Thread(None, self.FindFiles, name, args=(path, recursive, True))
        self.threads.Add(t)
        self.thread_count.Inc()

    ## File files and folders, which can optionally be done recursively.
    def FindFiles(self, path, recursive = True, threaded = False):

        found_files = []
        files = []
        try:
            files = os.listdir(path)
        except:
            self.failed_files.append(path)
            files = []
        for f in files:
            file_path = os.path.join(path, f) if path and path != "." else f
            if os.path.isdir(file_path):
                if f != '..' and f != '.':
                    found_files.append(file_path)
                    if recursive:
                        remaining_thread_count = self.max_threads - self.thread_count.value
                        if self.max_threads > 1 and remaining_thread_count > 0:
                            self.ThreadFindFiles(file_path)
                        else:
                            self.FindFiles(file_path, True, False)
            else:
                found_files.append(file_path)

        self.Append(found_files)

        if threaded:
            self.thread_count.Dec()

if __name__ == '__main__':

    argv = sys.argv[1:]
    recursive     = False
    show_failures = False
    silent        = False
    summary       = False
    max_threads   = max(1, multiprocessing.cpu_count() - 1)

    for arg in argv:
        if arg in ["-r", "--recursive"]:
            recursive = True
        elif arg == "--show-failures":
            show_failures = True
        elif arg == "--silent":
            silent = True
        elif arg == "--summary":
            summary = True
        elif arg.startswith("--threads="):
            max_threads = int(arg[10:])

    start_time = time.perf_counter()
    ff = ThreadedFileFinder(".", recursive, max_threads)
    ff.Join()
    end_time = time.perf_counter()

    if not silent:
        for f in ff.files:
            print(f)

        if show_failures and ff.failed_files:
            print("Failures:")
            for f in ff.failed_files:
                print(f)

    if summary:
        print("Number of files {}".format(len(ff.files)))
        if show_failures:
            print("Number of failed files {}".format(len(ff.failed_files)))
        print("Elapsed time {}".format(end_time - start_time))
