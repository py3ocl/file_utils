#!/usr/bin/env python

# Copyright 2019 Colin Girling
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import time

class FileFinder:

    def __init__(self, path, recursive = True, func = None, failure_func = None):
        self.files = []
        self.failed_files = []
        self.file_count = 0
        self.failure_file_count = 0
        self.func = func
        self.failure_func = failure_func
        self.FindFiles(path, recursive)

    ## Clear the list of files.
    def Clear(self):
        self.files = []

    ## Either add filename to self.files or use self.func to manage filename.
    def RecordFile(self, filename):
        if self.func:
            self.func(filename)
        else:
            self.files.append(filename)
        self.file_count += 1

    ## File files and folders, which can optionally be done recursively.
    def FindFiles(self, path, recursive = True):

        files = []
        try:
            files = os.listdir(path)
        except:
            if self.failure_func:
                self.failure_func(path)
            else:
                self.failed_files.append(path)
            self.failure_file_count += 1
        for f in files:
            file_path = os.path.join(path, f) if path and path != "." else f
            if os.path.isdir(file_path):
                if f != '..' and f != '.':
                    self.RecordFile(file_path)
                    if recursive:
                        self.FindFiles(file_path, True)
            else:
                self.RecordFile(file_path)

def PrintFailure(msg):
    sys.stderr.write("FAILURE: {}\n".format(msg))

if __name__ == '__main__':

    argv = sys.argv[1:]
    recursive     = len(sys.argv) > 0 and ("-r" in argv or "--recursive" in argv)
    show_failures = len(sys.argv) > 0 and "--show-failures" in argv
    silent        = len(sys.argv) > 0 and ("--silent" in argv or "--quiet" in argv)
    summary       = len(sys.argv) > 0 and "--summary" in argv
    print_func    = None if silent else print
    failure_func  = None if silent else PrintFailure

    start_time = time.perf_counter()
    ff = FileFinder(".", recursive, print_func, failure_func)
    end_time = time.perf_counter()

    if summary:
        print("Number of files {}".format(ff.file_count))
        if show_failures:
            print("Number of failed files {}".format(ff.failure_file_count))
        print("Elapsed time {}".format(end_time - start_time))
